package com.dream.resttemplate;

import org.springframework.stereotype.Component;

@Component
public class strPojo {

	String str;

	public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}

	@Override
	public String toString() {
		return "strPojo [str=" + str + "]";
	}
	
	
}